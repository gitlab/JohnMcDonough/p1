package edu.bu.ec504.hw3.Tests;

import edu.bu.ec504.hw3.bst.BSTSet;

import java.util.Arrays;
import java.util.Set;

public class TestBSTSet {

    public static void main(String[] args) {
        Set<String> strSet = new BSTSet<String>();
        String strs[] = new String[]{"Alpha","Beta","Gamma","Delta","Iota"};
        strSet.addAll(Arrays.asList(strs));

        // check size
        System.out.print("Testing size");
        if (strSet.size()!=strs.length) {
            throw new RuntimeException("BSTSet size is "+strSet.size()+" and not the expected "+strs.length);
        }
        System.out.println("... ok");

        // use contains
        System.out.print("Testing contains");
        if (!strSet.contains("Alpha")
            || !strSet.contains("Beta")
            || !strSet.contains("Delta")
            || strSet.contains("Epsilon"))
            throw new RuntimeException("BSTSet not functioning as a set - provides incorrect responses.");
        System.out.println("... ok");

        // use set iteration
        System.out.print("Testing iteration");
        boolean found=false;
        for (String str: strSet) {
            if (str.equals("Iota"))
                found=true;
        }
        if (!found) {
            throw new RuntimeException("Set iteration failed.");
        }
        System.out.println("... ok");
    }




}
