package edu.bu.ec504.hw3.bst;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

/**
 * An extension of BST that supports
 * @param <keyType>
 */
public class BSTSet<keyType extends Comparable<keyType>> extends BST<keyType> implements Set<keyType> {

  /**
   * @inheritDoc
   */
  @Override
  public int size() {
    return 0;
  }

  /**
   * @inheritDoc
   */
  @Override
  public boolean isEmpty() {
    return false;
  }

  /**
   * @inheritDoc
   */
  @Override
  public Iterator<keyType> iterator() {
    return null;
  }

  /**
   * @inheritDoc
   * @implNote You do not need to implement this for the homework.
   */
  @Override
  public Object[] toArray() {
    return new Object[0];
  }

  /**
   * @inheritDoc
   * @implNote You do not need to implement this for the homework.
   */
  @Override
  public <T> T[] toArray(T[] a) {
    return null;
  }

  /**
   * @inheritDoc
   * @implNote You do not need to implement this for the homework.
   */
  @Override
  public boolean remove(Object o) {
    return false;
  }

  /**
   * @inheritDoc
   */
  @Override
  public boolean containsAll(Collection<?> c) {
    return false;
  }

  /**
   * @inheritDoc
   */
  @Override
  public boolean addAll(Collection<? extends keyType> c) {
    return false;
  }

  /**
   * @inheritDoc
   * @implNote You do not need to implement this for the homework.
   */
  @Override
  public boolean retainAll(Collection<?> c) {
    return false;
  }

  /**
   * @inheritDoc
   * @implNote You do not need to implement this for the homework.
   */
  @Override
  public boolean removeAll(Collection<?> c) {
    return false;
  }

  /**
   * @inheritDoc
   * @implNote You do not need to implement this for the homework.
   */
  @Override
  public void clear() {

  }
}
